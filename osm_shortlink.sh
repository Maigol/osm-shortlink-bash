#!/bin/bash
# Bash script to generate OpenStreetMap Shortlinks
# Based on a Python implementation by Mdornseif:
#  https://gist.github.com/mdornseif/5652824
#
# Usage:
#  osm_shortlink.sh latitude longitude [zoom]

lat=${1}
lon=${2}
zoom=${3:-16}
chars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_~"

x=$(echo "($lon + 180) * 2^32 / 360" | bc )
y=$(echo "($lat +  90) * 2^32 / 180" | bc )
y=$((y+1))

# Combine 2 32 bit integers to a 64 bit integer

code=0
for i in $(seq 31 -1 1); do
	code=$(( (code << 1) | ((x >> i) & 1) ))
	code=$(( (code << 1) | ((y >> i) & 1) ))
done

# Add eight to the zoom level, which approximates an accuracy of
#  one pixel in a tile.

str=""
n=$(( (zoom + 8) / 3 ))
for i in $(seq 0 $((n-1))); do
	digit=$(( (code >> (56 - 6 * i)) & 0x3f ))
	str=${str}${chars:digit:1}
done

# Append characters onto the end of the string to represent
#  partial zoom levels (characters themselves have a granularity
#  of 3 zoom levels).

n=$(( (zoom + 8) % 3 ))
for i in $(seq $n); do
	str=${str}-
done

echo https://osm.org/go/$str?m
